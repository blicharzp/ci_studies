.. src documentation master file, created by
   sphinx-quickstart on Thu Oct 29 23:59:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to src's documentation!
===============================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   src


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
