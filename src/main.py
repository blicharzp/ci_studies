"""
main.py
====================================
The core module.
"""

from typing import Sequence, TypeVar


T = TypeVar("T")


def foo() -> int:
    """Returns 42, introduced for learning purpose

    :return: Value 42
    :rtype: Int
    """
    return 42


def bar(arg: int) -> int:
    """Returns multiplied by 10 input `arg` parameter

    :param arg: Input argument
    :type arg: Int
    :return: 10 * `arg`
    :rtype: Int
    """
    return 10 * arg


def reduce(arg: Sequence[T], begin: T) -> T:
    """Returns sum of `arg` sequance increased by `begin`

    :param arg: Input sequance
    :type arg: Sequence[T]
    :param begin: First value
    :type begin: T
    :return: Sum of given sequence
    :rtype: T
    """
    return sum(arg) + begin


def main():
    pass  # pragma: no cover


if __name__ == "__main__":
    main()  # pragma: no cover
