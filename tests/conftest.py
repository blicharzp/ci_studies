import pytest


@pytest.hookimpl()
def pytest_sessionstart(session):
    print("Hook")


@pytest.fixture
def dummy():
    def subtract(collection):
        if len(collection) == 0:
            return 0
        head, *tail = collection
        for element in tail:
            head -= element
        return head
    print("begin")
    yield subtract
    print("after")
