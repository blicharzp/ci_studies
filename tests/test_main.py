import pytest
from unittest.mock import patch
from src import main


def test_foo():
    assert 42 == main.foo()


def test_bar():
    assert 420 == main.bar(main.foo())


def test_reduce():
    assert 42 == main.reduce(range(6), 27)


@pytest.mark.parametrize("test_input, expected", [([4, 4], 8), ([2, 4], 6)])
def test_parametrize(test_input, expected):
    assert sum(test_input) == expected


@pytest.mark.parametrize("test_input, expected", [([4, 4], 0), ([2, 4], -2)])
def test_patch(test_input, expected, dummy):
    with patch("__main__.sum", side_effect=dummy) as sum:
        assert sum(test_input) == expected
        sum.assert_called_once()
